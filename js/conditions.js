let myVariable = 1;
//truthy ou falsy

/*
pour tester la valeur d'une variable, on
utilise des opérateurs logique.
Si je veux voir si maVariable vaut 1 j'utilise
myVariable === 1

Bien utiliser === et pas == ni =, cela permet de
tester la strict égalité (en prenant en compte le
typage)
*/


//ne pas utiliser le == 
console.log(myVariable == true); //true
//utiliser uniquement le ===
console.log(myVariable === true); //false

//On peut utiliser !== pour tester la non-égalité
console.log(myVariable !== 2); //true
//On peut tester si une valeur est supérieure/inférieure à une autre
console.log(myVariable < 2); //true
console.log(myVariable >= 2); //false

//On peut lié deux tests ensemble avec un &&
//Le test ne passe que si les deux parties sont true
console.log(true && false); //false
console.log(true && true); //true
//Ici, on vérifie si myVariable est comprise entre 1 et 10
console.log( myVariable >= 1 && myVariable <= 10 ); //true si la variable vaut entre 1 et 10

//On peut aussi lié avec un ||
//Le test passera tant qu'une des deux partie est true
console.log(true || false); //true
console.log(true || true); //true
console.log(false || false); //false

//Ici, on vérifie si myVariable vaut 1 ou 5
console.log(myVariable === 1 || myVariable === 5); //true si la variable vaut 1 ou 5

//Tester si myVariable vaut entre -1 et 1 ou alors qu'elle vaut 10
console.log(myVariable >= -1 && myVariable <= 1 || myVariable === 10);

/*
Les conditions : 
On peut utiliser les if pour exécuter des blocs de code, représentés
par des accolades, seulement si une condition est true.
*/
if( myVariable === 10) {
    //Ne s'exécutera que si myVariable vaut 10
}
//Un else après un if permet d'exécuter un  autre bloc de code si la condition n'est pas vérifiée
if( myVariable > 0 ) {
    //Ne s'exécutera que si myVariable est supérieure à 0
} else {
    //Ne s'exécutera que si myVariable est inférieure ou égale à 0
}

//On peut utiliser un else if pour définir une autre condition si la précédente n'est pas vérifiée
if( myVariable === 1 ) {
    //Ne s'exécutera que si myVariable vaut 1
} else if( myVarialbe === 2) {
    //Ne s'exécutera que si myVariable vaut 2
} else {
    //Ne s'exécutera que si myVariable vaut quoique ce soit d'autre
}

/*
La portée des variables.
Une variable n'est disponible que dans le bloc où elle a été
déclarée et dans les bloc enfant, mais pas dans les blocs parents.
Si je déclare une variable à la racine de mon fichier, elle sera
accessible partout.
Par contre si je déclare une variable à l'intérieur d'un if, cette
variable ne sera accessible qu'à l'intérieur de ce if, ou dans les
blocs à l'intérieur du if.
Exemple ci dessous avec les console log qui représente les variables
accessibles.
*/
console.log(myVariable);
if( myVariable === 1 ) {
    let scopedVariable = 10;
    console.log(myVariable);
    console.log(scopedVariable);
    if(myVariable === 3) {
        let subScopedVariable = 10;
        console.log(myVariable);
        console.log(scopedVariable);
        console.log(subScopedVariable);
        
    }
}
